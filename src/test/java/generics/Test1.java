package generics;

import org.example.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Test1 {
    @Test
    public void BaseBallTeamTest(){
        BaseballTeam phillies1 = new BaseballTeam("Philadelphia Phillies");
        BaseballTeam astros1 = new BaseballTeam("Houston Astros");
        Main.scoreResults(phillies1, 2, astros1, 0);

        SportsTeam phillies = new SportsTeam("Philadelphia Phillies");
        SportsTeam astros = new SportsTeam("Houston Astros");
        Main.scoreResults(phillies, 2, astros, 0);

        var harper = new BaseballPlayer("B Harper", "Right Fielder");
        var marsh = new BaseballPlayer("B Marsh", "Right Fielder");

        phillies.addTeamMember(harper);
        phillies.addTeamMember(marsh);

        phillies.listTeamMembers();

        SportsTeam afc = new SportsTeam("Adelaide Crows");
        var player = new FootballPlayer("Tex Walker", "Center half forward");
        afc.addTeamMember(player);
        afc.listTeamMembers();

        Team<CricketPlayer> rr = new Team("Rajasthan Royals");
        var cricketPlayer = new CricketPlayer("Sanju Samson", "One Down");
        rr.addTeamMember(cricketPlayer);
        rr.listTeamMembers();

        Team<CricketPlayer> rcb = new Team("Royal Challengers Bangalore");
        var cricketPlayer2 = new CricketPlayer("FAF Duplesis", "Opener");
        rcb.addTeamMember(cricketPlayer2);
        rcb.listTeamMembers();

        Main.scoreResults(rr, 210, rcb, 176);

    }
}
