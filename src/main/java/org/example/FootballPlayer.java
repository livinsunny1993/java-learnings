package org.example;

public record FootballPlayer(String name, String position) implements Player {
}
