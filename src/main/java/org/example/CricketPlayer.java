package org.example;

public record CricketPlayer(String name, String position) implements Player {
}
