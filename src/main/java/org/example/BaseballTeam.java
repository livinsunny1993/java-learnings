package org.example;

import java.util.List;
import java.util.ArrayList;

public class BaseballTeam {
    private String teamName;
    private List<BaseballPlayer>  teamMembers = new ArrayList<>();
    private int totalWins = 0;
    private int totalLosses = 0;
    private int totalTies = 0;

    public BaseballTeam(String teamName){
        this.teamName = teamName;
    }
    public boolean addTeamMember(BaseballPlayer player){
//        if(!teamMembers.contains(player)){
//            teamMembers.add(player);
//        }
        return teamMembers.contains(player) ? false : teamMembers.add(player);
    }

    public void listTeamMembers(){
        System.out.println(teamName + " Rooster:");
        System.out.println(teamMembers);
    }

    public int ranking(){
        return totalLosses * 2 + totalTies + 1;
    }

    public String setScore(int ourScore, int theirScore){
        String message = "";
        if(ourScore > theirScore){
            totalWins++;
            message = "beat";
        }else if(ourScore == theirScore){
            totalTies++;
            message = "tied";
        }else{
            totalLosses++;
            message = "lost to";
        }
        return message;
    }

    @Override
    public String toString(){
        return teamName + " [ rank: " + ranking() + " ]";
    }
}
